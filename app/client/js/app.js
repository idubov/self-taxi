'use strict';

var SelfTaxi = angular.module('SelfTaxi', ['ngRoute', 'ui.bootstrap']);

// Controllers ----------------------------------------------------------------------------

SelfTaxi.controller('MainController', ['$scope', '$http', '$modal', function ($scope, $http, $modal) {
  
  $http.get('/api/trips').success(function (data) {
    $scope.tripsTaxi = data.filter(function (item) {
      return item.type === 0;
    });
    $scope.tripsPassengers = data.filter(function (item) {
      return item.type === 1;
    });
    $scope.deleteTrip = function (tripId, tripIdx, tripType) {
      $scope.tripToDelete = {
        id: tripId,
        idx: tripIdx,
        type: tripType
      };
      $modal.open({
        templateUrl: '/views/delete.html',
        controller: 'DeleteController',
        scope: $scope
      })
    };
    $scope.deleted = [];
  });
  
}]);

SelfTaxi.controller('PostTripController', ['$scope', '$http', '$location',
function ($scope, $http, $location) {
  var path = $location.path();
  var type = path === '/posttaxi' ? 0 : path === '/postpassenger' ? 1 : -1;
  if (type === 0) {
    $scope.title = 'Хочу взять попутчиков';
    $scope.howMuch = 'Сколько мест';
    $scope.willPay = 'Стоимость';
    $scope.info = 'Для девушек - бесплатно)';
  } else if (type === 1) {
    $scope.title = 'Ищу машину';
    $scope.howMuch = 'Сколько нас';
    $scope.willPay = 'Заплатим';
    $scope.info = 'Обожаю музыку, не терплю табачный дым';
  }
  angular.extend($scope, {
    form: {},
    go: function () {
      var trip = $scope.form;
      trip.type = type;
      $http.post('/api/trips', trip);
      $location.path('/');
    }
  });
}]);

SelfTaxi.controller('DeleteController', ['$scope', '$modalInstance', '$http', function ($scope, $modalInstance, $http) {
  $scope.secret = '';
  $scope.wrongSecret = false;
  $scope.ok = function () {
    if ($scope.secret) {
      $http['delete']('/api/trips/' + $scope.tripToDelete.id + '/' + $scope.secret)
      .success(function () {
        $scope.tripToDelete.type ?
          $scope.tripsPassengers.splice($scope.tripToDelete.tripIdx, 1) :
          $scope.tripsTaxi.splice($scope.tripToDelete.tripIdx, 1);
        $modalInstance.close();
      })
      .error(function (error, status) {
        if (status = 403) {
          $scope.wrongSecret = true;
        }
      });
    }
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}]);

// Config ---------------------------------------------------------------------------------

SelfTaxi.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
  $locationProvider.html5Mode(true);
  $routeProvider.when('/', {
    controller: 'MainController',
    templateUrl: 'views/main.html'
  }).when('/posttaxi', {
    controller: 'PostTripController',
    templateUrl: 'views/posttrip.html'
  }).when('/postpassenger', {
    controller: 'PostTripController',
    templateUrl: 'views/posttrip.html'
  }).when('/about', {
    templateUrl: 'views/about.html'
  }).when('/contacts', {
    templateUrl: 'views/contacts.html'
  });
}]);
