var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var TripSchema = new Schema({
  where: String,
  when: String,
  places: Number,
  cost: Number,
  contacts: String,
  type: Number, // 0 - taxi, 1 - passenger
  secret: String,
  info: String
});

module.exports = mongoose.model('Trip', TripSchema);
