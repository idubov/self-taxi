// base setup
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

// db
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/selftaxi');

// models
var Trip = require('./models/trip');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
        
var port = process.env.PORT || 8010;

// routing
var router = express.Router();

router.route('/trips')

  .get(function (req, res) {
    Trip.find(function(err, trips) {
      if (err) res.send(err);
      res.json(200, trips);
    });
  })

  .post(function (req, res) {
    var trip = new Trip(req.body);
    trip.save(function (err) {
      if (err) {
        res.send(err);
      }
      res.json(201, trip);
    });
  });

router.route('/trips/:id/:secret')

  .delete(function (req, res) {
    Trip.findById(req.params.id, function (err, trip) {
      if (err) {
        res.send(err);
      }
      if (!trip) {
        res.send(404);
        
      } else {
        if (req.params.secret !== trip.secret) {
          res.send(403);
        } else {
          trip.remove(function (err) {
            if (err) {
              res.send(err);
            } else {
              res.send(204);
            }
          });
        }
      }
      
    });
  });

app.use('/api', router);
app.use(express.static(__dirname + '/client'));
app.all('/*', function(req, res, next) {
    // Just send the index.html for other files to support HTML5Mode
    res.sendfile('index.html', { root: __dirname + '/client'});
});

// minify static files
var compressor = require('node-minify');
new compressor.minify({
  type: 'yui-css',
  fileIn: ['app/client/css/app.css'],
  fileOut: 'app/client/css/taxi.css'
});
new compressor.minify({
  type: 'gcc',
  fileIn: ['app/client/bower_components/angular/angular.min.js',
           'app/client/bower_components/angular-route/angular-route.min.js',
           'app/client/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
           'app/client/js/app.js'],
  fileOut: 'app/client/js/taxi.js'
});

// go
app.listen(port);
console.log('ok, we started');
